const electron = require('electron');
const app = electron.app;
const path = require('path');
const url = require('url');
const BrowserWindow = electron.BrowserWindow;
var mainWindow;
app.on('ready',function(){
  mainWindow = new BrowserWindow({width: 700, height: 610, resizable:false});
  //mainWindow.webContents.openDevTools()  
  //mainWindow.loadURL('https://github.com');
   mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'weather.html'),
    protocol: 'file:',
    slashes: true
  })
   
   
);

  app.on('window-all-closed', () => {
    app.quit()
    if(process.platform !== 'darwin'){
        app.quit()
    }
})

  
});