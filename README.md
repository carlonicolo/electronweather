# Electron weather App
Electron weather app is a simple electron app that uses openweather API for showing the weather info about a selected city.

## How to install and run
First of all you need to supply your OpenWeatherMap API key (you can find it [here](https://openweathermap.org/api)) and use it in the weather.js,:  

```
/**
 * Function used to retreive info from JSON 
 * @param {string} val is the city for which we want retreive weather info
 */
function findWeather(val){
    //retrieve weather information from the API
    console.log("Valore di val: ",val);
    
    var api_key = "";
    api_url = 'http://api.openweathermap.org/data/2.5/weather?q='+val+'&mode=json&appid='+api_key+'&units=metric'
```
At this point, run ```npm install``` in the project directory. Now you can package the WeatherApp for your preferred platform using the command:  

```
electron-packager .
```

## Demo
![alt text](img/electronweather.gif "Project Demo")

