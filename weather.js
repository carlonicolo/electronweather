/**
 * Function to retreive info from openwheater API
 * @param {string} theUrl 
 */
function httpGet(theUrl){
    var xmlHttp = null;
    xmlHttp = new XMLHttpRequest()
    xmlHttp.open("GET", theUrl, false)
    xmlHttp.send(null)
    return xmlHttp.responseText
}

/**
 * Function used to retreive info from JSON 
 * @param {string} val is the city for which we want retreive weather info
 */
function findWeather(val){
    //retrieve weather information from the API
    console.log("Valore di val: ",val);
    var api_key = "";
    api_url = 'http://api.openweathermap.org/data/2.5/weather?q='+val+'&mode=json&appid='+api_key+'&units=metric'
    var http_response = httpGet(api_url)
    //parsing data returned as JSON and setting text fields
    var http_response_json = JSON.parse(http_response)
    document.getElementById("Temperature").innerHTML = http_response_json.main.temp+"&#8451;"
    document.getElementById("Windspeed").innerHTML = http_response_json.wind.speed+" km/h"
    document.getElementById("temp_max").innerHTML = http_response_json.main.temp_max+"&#8451;"
    document.getElementById("temp_min").innerHTML = http_response_json.main.temp_min+"&#8451;"
    document.getElementById("Pressure").innerHTML = http_response_json.main.pressure+" hPa"

    var id = http_response_json.weather[0].id
    console.log("ID",id);

    var description_svg = checkWeatherId(id);
    document.getElementById("description").innerHTML = description_svg
    //document.getElementById("description").innerHTML = http_response_json.weather[0].description
    document.getElementById("humidity").innerText = http_response_json.main.humidity+"%"
    document.getElementById("name").innerText = http_response_json.name
}

/**
 * Check the id code and create a link to the icon image contained in the img folder
 * @param {int} id is the weather code used in openweather API 
 */
function checkWeatherId(id){
    //check the id number to choose the right img to display
    console.log("Param value id: ", id)
    
    var icon_description = "";
    //200 Thunderstorm group, 300 Drizzle group, 700 Atmosphere
    var check_group = parseInt(id/100)
    console.log("Check group value: ", check_group);
    if(check_group == 7){
        icon_description = "<img src='img/700.svg' />"
        return icon_description
    }else if(check_group == 3){
        icon_description = "<img src='img/300.svg' />"
        return icon_description
    }else if(check_group == 2){
        icon_description = "<img src='img/200.svg' />"
        return icon_description
    }
    else{
        var icon_name = id+".svg";
        icon_description = "<img src='img/"+icon_name+"' />"
        console.log("Check icon_description varibale: ", icon_description)
        return icon_description
    }
    

}